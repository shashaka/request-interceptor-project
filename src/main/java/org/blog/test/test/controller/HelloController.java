package org.blog.test.test.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping(value = "/hello",
            method = RequestMethod.GET)
    public String getHello() {
        return "hello";
    }

    @RequestMapping(value = "/bye",
            method = RequestMethod.GET)
    public String getbye() {
        return "bye";
    }
}